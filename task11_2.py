"""
Создайте класс, описывающий отзыв к книге. Добавьте в класс книги поле – список отзывов.
Сделайте так, что при выводе книги на экран при помощи функции print также будут выводиться отзывы к ней.

---------------------------------------------------------------------------------------------------------
Використовуємо взаємодію між класами "Асоціація"
(один з класів місти поля іншого)
Існує 2 види Асоціації: Композиція і Агрегація
Композиція - коли один клас не існує без іншого і його елементи створюються в конструкторі іншого
Агрегація - коли елементи повязаного класу створені окремо і передаються в конструктор головного як параметри
В даному випадку використовуємо Агрегацію
"""


class Response:
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return f'Text of response: "{self.text}"'


class Book:

    def __init__(self, author, name, year, genre, list_response):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre
        self.list_response = list_response

    def __eq__(self, other):
        return self.author == other.author and self.name == other.name

    def __ne__(self, other):
        return self.author != other.author or self.name != other.name

    def __repr__(self):
        return f'"{self.name}" by {self.author}, in the genre {self.genre}'

    def __str__(self):
        str_book = f'"{self.name}" by {self.author}, written in {self.year}'
        for element in self.list_response:
            str_book += f"\n\t {element}"
        return str_book


response1 = Response("It's very good book!")
response2 = Response("It's very bad!")
response3 = Response("Boring...")

list_of_responses = [
    response1,
    response2,
    response3
]

book1 = Book("Henrikh Sienkovych", "With fire and sword", 1995, "historical novel", list_of_responses)

print(book1)
