"""
Создайте класс, описывающий книгу. Он должен содержать информацию об авторе, названии, годе издания и жанре.
Создайте несколько разных книг.
Определите для него операции проверки на равенство и неравенство, методы __repr__ и __str__.
"""


class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre

    def __eq__(self, other):
        return self.author == other.author and self.name == other.name

    def __ne__(self, other):
        return self.author != other.author or self.name != other.name

    def __repr__(self):
        return f'"{self.name}" by {self.author}, in the genre {self.genre}'

    def __str__(self):
        return f'"{self.name}" by {self.author}, written in {self.year}'


book1 = Book("Henrikh Sienkovych", "With fire and sword", 1995, "historical novel")
book2 = Book("Arthur Conan Doyle", "The adventures of Sherlock Holmes", 1990, "detective")
book3 = Book("Ayn Rand", "The Atlas straightened his shoulders", 2020, "dystopia")
book4 = Book("Ayn Rand", "The Atlas straightened his shoulders", 2010, "dystopia")
book5 = Book("Henrikh Sienkovych", "Crusaders", 1995, "historical novel")

book_list = [
    book1,
    book2,
    book3,
    book4,
    book5]

print("The list of books:")
for i in range(5):
    print(book_list[i])

print("\n Comparison of books:")
for i in range(5):
    for j in range(i+1, 5):
        if book_list[i] == book_list[j]:
            print(f" The books {book_list[i]} and {book_list[j]} is equal")
        if book_list[i] != book_list[j]:
            print(f" The books {book_list[i]} and {book_list[j]} is not equal")
