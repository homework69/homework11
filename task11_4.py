"""
Создайте класс, описывающий автомобиль. Создайте класс автосалона, содержащий в себе список автомобилей,
доступных для продажи, и функцию продажи заданного автомобиля.
"""


class Car:
    def __init__(self, brand, model, number=''):
        self.brand = brand
        self.model = model
        self.number = number

    def __str__(self):
        return f"{self.brand}, model {self.model} '{self.number}'"


class CarSaloon:
    def __init__(self, name, car_list):
        self.name = name
        self.car_list = car_list

    def sell(self, car):
        if car in self.car_list:
            self.car_list.remove(car)
            print(f"Sold car {car}")

    def print_list(self):
        print("List of available cars: ")
        for element in self.car_list:
            print(element)


car1 = Car("Porshe", "911", "AA 8888 KX")
car2 = Car("Fiat", "Tipo")
car3 = Car("Skoda", "Karoq", "KA 1212 KX")

list_cars = [
    car1,
    car2,
    car3
]

car_saloon = CarSaloon("Mirage", list_cars)

print("\nBefore selling")
car_saloon.print_list()
print("")
car_saloon.sell(car2)
print("\nAfter selling")
car_saloon.print_list()
